#include <math.h>
#include "main.h"

Engine::Engine(int screenWidth, int screenHeight) : gameStatus(STARTUP),
player(NULL), map(NULL), fovRadius(10),
screenWidth(screenWidth), screenHeight(screenHeight), level(1) {
	TCODConsole::initRoot(screenWidth, screenHeight, "My Roguelike Project", false);
	gui = new Gui();
}

void Engine::init() {
	player = new Actor(40, 25, '@', "Player", TCODColor::white);
	player->destructible = new PlayerDestructible(50, 1, "Your cadaver");
	player->attacker = new Attacker(5);
	player->ai = new PlayerAi();
	player->container = new Container(26);
	actors.push(player);
	stairs = new Actor(0, 0, '>', "Stairs", TCODColor::white);
	stairs->blocks = false;
	stairs->fovOnly = false;
	actors.push(stairs);
	map = new Map(80, 43);
	map->init(true);
	gui->message(TCODColor::red,
		"Welcome to the dungeons!");
	gameStatus = STARTUP;
}

Engine::~Engine() {
	term();
	delete gui;
}

void Engine::term() {
	actors.clearAndDelete();
	if (map) delete map;
	gui->clear();
}

void Engine::update() {
	if (gameStatus == STARTUP) map->computeFov();
	gameStatus = IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS | TCOD_EVENT_MOUSE, &lastKey, &mouse);
	if (lastKey.vk == TCODK_ESCAPE) {
		save();
		load();
	}
	player->update();
	if (gameStatus == NEW_TURN) {
		for (Actor **iterator = actors.begin(); iterator != actors.end();
			iterator++) {
			Actor *actor = *iterator;
			if (actor != player) {
				actor->update();
			}
		}
	}
}

void Engine::render() {
	TCODConsole::root->clear();
	// draw the map
	map->render();
	// draw the actors
	for (Actor **iterator = actors.begin();
		iterator != actors.end(); iterator++) {
		Actor *actor = *iterator;
		if (actor != player && ((!actor->fovOnly && map->isExplored(actor->x, actor->y))
			|| map->isInFov(actor->x, actor->y))) {
			actor->render();
		}
	}
	player->render();
	// show the player's stats
	gui->render();
}

void Engine::sendToBack(Actor *actor) {
	actors.remove(actor);
	actors.insertBefore(actor, 0);
}

void Engine::nextLevel() {
	level++;
	player->destructible->heal(player->destructible->maxHp / 2);
	gui->message(TCODColor::red, "You have been healed, proceed through this level");
	delete map;
	// delete all actors but player and stairs
	for (Actor **it = actors.begin(); it != actors.end(); it++) {
		if (*it != player && *it != stairs) {
			delete *it;
			it = actors.remove(it);
		}
	}
	// create a new map
	map = new Map(80, 40);
	map->init(true);
	gameStatus = STARTUP;
}