Code for my final year project I did at university. 

It is a Rogue-like dungeon crawler made entirely in C++.

The level design is all procedural generation which was the main focus of my project. So the levels are always different and gives a different experience every time.